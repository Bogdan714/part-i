package com.company;

public class Main {

    public static void main(String[] args) {
        Cafe cafe = new Cafe();
        cafe.showMenu();
        cafe.takeOrder();
    }
}
