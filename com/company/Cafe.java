package com.company;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Cafe {
    Map<String, Float> dishes = new HashMap<>();

    public Cafe() {
        dishes.put("borscht", 15f);
        dishes.put("lard", 18f);
        dishes.put("buckwheat", 13f);
        dishes.put("bread", 5f);
        dishes.put("sour cream", 5f);
    }

    public void showMenu() {
        System.out.println("__MENU_________");
        for (Map.Entry<String, Float> stringIntegerEntry : dishes.entrySet()) {
            System.out.println(stringIntegerEntry.getKey() + " - " + stringIntegerEntry.getValue() + " UAH");
            System.out.println("----------");
        }
    }

    public void takeOrder() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("let's order something!");
        Map<String, Float> order = new HashMap<>();
        String dishName;
        int count;
        do {
            do {
                System.out.println("Enter some dish from Menu (\'stop\' to stop)");
                dishName = scanner.nextLine();
                if (dishName.equalsIgnoreCase("stop")) {
                    break;
                }
            } while (!dishes.containsKey(dishName.toLowerCase()));
            if (dishName.equalsIgnoreCase("stop")) {
                break;
            }
            do {
                System.out.println("How many?");
                count = Integer.parseInt(scanner.nextLine());
            } while (count < 1 || count > 1000);
            order.put(dishName, count * dishes.get(dishName));
        } while (true);
        printCheck(order);
    }

    public static void printCheck(Map<String, Float> checkToPrint) {
        float total = 0;
        System.out.println("CHECK");
        System.out.println("________________");
        for (Map.Entry<String, Float> stringIntegerEntry : checkToPrint.entrySet()) {
            total += stringIntegerEntry.getValue();
            System.out.println(String.format("%s %.2f", stringIntegerEntry.getKey(), stringIntegerEntry.getValue()));
            System.out.println("-----");
        }
        System.out.println("TOTAL PRISE: " + total);
        System.out.println("________________");
    }
}
